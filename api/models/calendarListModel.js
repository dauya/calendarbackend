'use strict';
var faker = require('faker');


const dates =  [
    {
      id: 1,
      uuid: faker.random.uuid(),
      title: faker.lorem.words(),
      weekday: faker.date.weekday(),
      description: faker.lorem.sentences()
    },
    {
      id: 2,
      uuid: faker.random.uuid(),
      title: "lunch",
      weekday: faker.date.weekday(),
      description: "Go for lunc by 2pm"
    },
    {
      id: 3,
      uuid: faker.random.uuid(),
      title: faker.lorem.words(),
      weekday: faker.date.weekday(),
      description: faker.lorem.sentences()
    }
];

module.exports = dates;