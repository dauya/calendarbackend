'use strict';

var db = require('../models/calendarListModel.js');

exports.getDates = function(req, res) {
  res.json(db);
};


exports.getDateById = function(req, res) {
  var id = req.params.dateId;
  db.map((appointment) => {
    if (appointment.id == id) {
      return res.status(200).send(
        appointment
      ).end();
    }
  });
  return res.status(404).send({
    success: 'false',
    message: 'appointment does not exist',
  }).end();
};
