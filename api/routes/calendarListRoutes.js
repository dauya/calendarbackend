'use strict';
module.exports = function(app) {
  var calendatList = require('../controllers/calendarListController');

  // todoList Routes
  app.route('/api/v1/dates')
    .get(calendatList.getDates);


  app.route('/api/v1/dates/:dateId')
    .get(calendatList.getDateById);
};