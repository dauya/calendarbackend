var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET');
    next();
});

var routes = require('./api/routes/calendarListRoutes'); //importing route
routes(app); //register the route

var swaggerUi = require('swagger-ui-express'),
    swaggerDocument = require('./swagger.json');

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'}).end();
});



app.listen(port);

module.exports = app;