const supertest = require('supertest');
const should = require("should");
const request = supertest.agent("http://localhost:3000/api/v1");

/**
 * Testing get all appointments endpoint
 */
describe('GET /dates', function () {
    it('respond with json containing a list of all dates', function (done) {
        request
        .get('/dates')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it("should return 404",function(done){
	    request
	    .get("/random")
	    .set('Accept', 'application/json')
	    .expect('Content-Type', /json/)
	    .expect(404)
	    .end(function(err,res){
	      res.status.should.equal(404);
	      done();
	    });
	  })
});

/**
 * Testing get a appointment data endpoint by giving an existing appointment
 */
describe('GET /dates/:id', function () {
    it('respond with json containing a single appointment data', function (done) {
        request
        .get('/dates/2')
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200, done);
    });

    it("should return 404",function(done){
	    request
	    .get("/dates/4")
        .set('Accept', 'application/json')
        .expect('Content-Type', /json/)
	    .expect(404)
	    .end(function(err,res){
	      res.status.should.equal(404);
	      done();
	    });
    });
});